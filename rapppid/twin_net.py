#    twin_net.py
#    Contains an implementation of the Twin Neural Network Architecture
#    -----
#    RAPPPID - A method for the Regularised Automatic Prediction of Protein-Protein Interactions using Deep Learning
#    Copyright (C) 2021-2022  Joseph Szymborski
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from torch import nn, Tensor

from .encoders import Encoder
from .classifier import Classifier

from typing import Tuple


class TwinNet(nn.Module):
    def __init__(self,
                 encoder: Encoder,
                 classifier: Classifier
                 ):
        """
        The PyTorch Lightning module for a twin neural network.

        :param encoder: The encoder to use in the twin architecture.
        :param classifier: The classifier to use to predict interaction between the two latents
        """

        super(TwinNet, self).__init__()

        self.encoder = encoder
        self.classifier = classifier

    def forward(self, a: Tensor, b: Tensor) -> Tuple[Tensor, Tensor, Tensor]:
        z_a = self.encoder(a)
        z_b = self.encoder(b)

        y_hat_logit = self.classifier(z_a, z_b).float()

        return z_a, z_b, y_hat_logit
