#    __main__.py
#    CLI interface to RAPPPID
#    -----
#    RAPPPID - A method for the Regularised Automatic Prediction of Protein-Protein Interactions using Deep Learning
#    Copyright (C) 2021-2022  Joseph Szymborski
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import fire
from rich.console import Console
from rich.progress import track
import pytorch_lightning as pl
from lightning_lite.utilities.seed import seed_everything
from pytorch_lightning.callbacks import ModelCheckpoint
from pytorch_lightning.loggers import TensorBoardLogger
from pytorch_lightning.callbacks import StochasticWeightAveraging
from sklearn.metrics import roc_auc_score, average_precision_score, accuracy_score
from passlib import pwd
import sentencepiece as sp
import toml
import torch
import numpy as np
import tables as tb

import io
import os
import sys
import tempfile
from typing import Union, Optional, List
from pathlib import Path
from datetime import datetime

from .rapppid import RAPPPID
from .data import RapppidDataModule, LegacyRapppidDataModule
from .embed import EmbedderType
from .encoders import EncoderType
from .encoders.awd_lstm import BiReduceType
from .classifier import ClassifierType
from .optimizer import OptimizerType
from .logging import DictLogger


def _get_threads():
    """ Returns the number of available threads on a posix/win based system """
    if sys.platform == 'win32':
        return int(os.environ['NUMBER_OF_PROCESSORS'])
    else:
        return int(os.popen('grep -c cores /proc/cpuinfo').read())


def test_model(console, model_path, onnx_path, data_module):
    #f'{log_path}/chkpts/{model_name}.ckpt'
    model = RAPPPID.load_from_checkpoint(model_path).eval().to('cuda')

    test_outs = []
    test_ys = []

    for batch in track(data_module.test_dataloader(), total=len(data_module.test_dataloader()),
                       description='Running Tests'):
        a, b, y = batch

        a = a.to('cuda')
        b = b.to('cuda')

        z_a = model(a)
        z_b = model(b)

        y_hat = torch.sigmoid(model.class_head(z_a, z_b).float()).flatten().cpu().detach().numpy().astype(
            np.float32).tolist()

        test_outs += y_hat
        test_ys += y.cpu().numpy().astype(int).tolist()

    console.log('Trying to serialize ONNX model...')
    try:
        torch.onnx.export(model, a[0, :, :].unsqueeze(0), str(onnx_path), verbose=True,
                          opset_version=12)
        console.log("Succeeded in saving ONNX")
    except Exception as e:
        console.log("Failed to save ONNX")
        console.log(e)

    try:
        test_auroc = roc_auc_score(test_ys, test_outs)
    except ValueError as e:
        test_auroc = -1

    try:
        test_apr = average_precision_score(test_ys, test_outs)
    except ValueError as e:
        test_apr = -1

    try:
        test_acc = accuracy_score(test_ys, (np.array(test_outs) > 0.5).astype(int))
    except ValueError as e:
        test_acc = -1

    test_results = {
        'results': test_outs,
        'labels': test_ys,
        'scores': {
            'auroc': test_auroc,
            'apr': test_apr,
            'acc': test_acc
        }
    }

    return test_results


def train_sentencepiece(console, seed: int, dataset_path: Path, vocab_size: int, model_file: Path):
    console.log('Creating sentencepiece token model')

    sp.set_random_generator_seed(seed)

    console.log('Loading dataset...')
    dataset = tb.open_file(str(dataset_path))

    tf = tempfile.NamedTemporaryFile('w+t')

    console.log('Writing sequences to temp file...')
    for sequence in track(dataset.root['sequences'], total=len(dataset.root['sequences'])):
        tf.write(sequence['sequence'].decode('utf8') + '\n')

    console.log('Building SentencePiece model...')

    model_data = io.BytesIO()

    sp.SentencePieceTrainer.train(input=tf.name,
                                   model_writer=model_data,
                                   vocab_size=vocab_size,
                                   character_coverage=1.,
                                   bos_id=-1,
                                   eos_id=-1,
                                   pad_id=0,
                                   unk_id=1,
                                   )

    with open(model_file, 'wb') as f:
        f.write(model_data.getvalue())

    tf.close()


def main(seed: int, optimizer_num_epochs: int, batch_size: int, optimizer_lr: float, trunc_len: int = 1500,
         gpus: Union[int, List[int]] = 1, log_path: Path = Path('.'), train_path: Optional[Path] = None,
         val_path: Optional[Path] = None, test_path: Optional[Path] = None, seqs_path: Optional[Path] = None,
         dataset_path: Optional[Path] = None, spm_path: Optional[Path] = None, c_type: Optional[int] = None,
         num_threads: Union[int, str, float] = 'most', transfer_path: Optional[Path] = None,
         embedder_type: EmbedderType = EmbedderType.DROP, vocab_size: int = 250, embedding_size: int = 128,
         embedding_droprate: Optional[float] = 0.2, encoder_type: EncoderType = EncoderType.AWDLSTM,
         bi_reduce_type: Optional[BiReduceType] = BiReduceType.LAST, encoder_num_layers: Optional[int] = None,
         encoder_dropout_rate: Optional[float] = None, encoder_variational_dropout: Optional[bool] = None,
         classifier_type: ClassifierType = ClassifierType.MULT, classifier_num_layers: Optional[int] = None,
         classifier_dropout_rate: Optional[float] = None, classifier_variational_dropout: Optional[bool] = None,
         optimizer_type: OptimizerType = OptimizerType.RANGER21, optimizer_weight_decay: Optional[float] = None,
         optimizer_swa: Optional[bool] = None, accelerator: str = 'gpu', devices: Union[List[int], int] = 0):

    console = Console()
    console.print("[red] ______________________________________________________")
    console.print("[red] ___  __ \__    |__  __ \__  __ \__  __ \___  _/__  __ \ ")
    console.print("[red] __  /_/ /_  /| |_  /_/ /_  /_/ /_  /_/ /__  / __  / / /")
    console.print("[red] _  _, _/_  ___ |  ____/_  ____/_  ____/__/ /  _  /_/ / ")
    console.print("[red] /_/ |_| /_/  |_/_/     /_/     /_/     /___/  /_____/  ")
    console.print("")
    console.print("[white]RAPPPID V2.0")
    console.print("[white]Copyright © 2021-2022 Joseph Szymborski, Licensed under the AGPL v3")
    console.print("")
    console.print("Kindly cite:")
    console.print("Szymborski J, Emad A. RAPPPID: towards generalizable protein interaction prediction with AWD-LSTM "
                  "twin networks. [i]Bioinformatics[/i]. 2022 Aug 10;38(16):3958-3967. doi: "
                  "10.1093/bioinformatics/btac429. PMID: 35771595.")
    console.print("")

    seed_everything(seed, workers=True)

    console.print("")

    # type-cast Path and Bool variables
    # Required when taking args from the CLI
    log_path = Path(log_path)
    train_path = Path(train_path) if train_path is not None else None
    val_path = Path(val_path) if val_path is not None else None
    test_path = Path(test_path) if test_path is not None else None
    seqs_path = Path(seqs_path) if seqs_path is not None else None
    dataset_path = Path(dataset_path) if dataset_path is not None else None
    spm_path = Path(spm_path) if spm_path is not None else None
    encoder_variational_dropout = eval(encoder_variational_dropout) \
        if encoder_variational_dropout in ['False', 'True'] else encoder_variational_dropout
    classifier_variational_dropout = eval(classifier_variational_dropout) \
        if classifier_variational_dropout in ['False', 'True'] else classifier_variational_dropout
    optimizer_swa = eval(optimizer_swa) if optimizer_swa in ['False', 'True'] else optimizer_swa


    args_path = log_path / 'args'
    chkpt_path = log_path / 'chkpts'
    tb_path = log_path / 'tb_logs'
    onnx_path = log_path / 'onnx'
    sentencepiece_path = log_path / 'sentencepiece'

    os.makedirs(args_path, exist_ok=True)
    os.makedirs(chkpt_path, exist_ok=True)
    os.makedirs(tb_path, exist_ok=True)
    os.makedirs(onnx_path, exist_ok=True)
    os.makedirs(sentencepiece_path, exist_ok=True)

    dt = datetime.now().isoformat().replace(':', '-').replace('T', '_')

    model_name = pwd.genphrase(length=2).replace(" ", "-")
    model_name = f"{dt}_{model_name}"
    console.log(f'[b][pink]Model Name[/pink][/b]: {model_name}')

    if num_threads == 'all':
        threads = _get_threads()
    elif num_threads == 'most':
        threads = max(1, _get_threads() - 2)
    elif num_threads == 'half':
        threads = max(1, _get_threads()//2)
    elif isinstance(num_threads, float) and 0 < num_threads < 1:
        threads = max(1, int(_get_threads() * num_threads))
    elif isinstance(num_threads, int) and num_threads > 0:
        threads = num_threads
    else:
        raise ValueError('Expected "all", "most", "half" or an integer greater than 0 for arg "num_threads"')

    console.log(f"Using {threads} thread{'s' if threads > 1 else ''}")

    # Do some validation on whether RAPPPID V1 datasets or RAPPPID V2 datasets are being used
    if (train_path is None or val_path is None or test_path is None or seqs_path is None) and dataset_path is None:
        raise ValueError('Either provide a "dataset_path" or all four of "val_path", "test_path", "train_path", and '
                         '"seq_path"')

    if spm_path is None:
        console.log('No sentencepiece model provided, training one.')
        spm_path = sentencepiece_path / f'{model_name}.spm'

        if dataset_path is not None:
            train_sentencepiece(console, 1, dataset_path, 250, spm_path)
        else:
            raise NotImplemented("Haven't implemented automatic sentencepiece for Legacy RAPPPID datasets")

    if dataset_path is not None:
        if c_type is None:
            raise ValueError('If "dataset_path" is specified, so must "c_type"')
        console.log('Using RAPPPID V2 Dataset format.')

        data_module = RapppidDataModule(batch_size, dataset_path, c_type, trunc_len, threads, spm_path, seed)
    else:
        console.log('Using Legacy RAPPPID V1 Dataset format')
        data_module = LegacyRapppidDataModule(batch_size, train_path, val_path, test_path, seqs_path, trunc_len,
                                              threads, spm_path, seed)

    data_module.setup()

    optimizer_steps_per_epoch = len(data_module.dataset_train) // batch_size

    if transfer_path:
        console.log(f'Loading existing weights from {transfer_path}')
        model = RAPPPID.load_from_checkpoint(str(transfer_path)).eval()
    else:

        model = RAPPPID(embedder_type, encoder_type, classifier_type, optimizer_type, vocab_size, embedding_size,
                        optimizer_lr, bi_reduce_type, encoder_num_layers, encoder_dropout_rate,
                        encoder_variational_dropout, embedding_droprate, classifier_num_layers, classifier_dropout_rate,
                        classifier_variational_dropout, optimizer_weight_decay, optimizer_steps_per_epoch,
                        optimizer_num_epochs)

    args = {
        "model_name": model_name,
        "seed": seed,
        "accelerator": accelerator,
        "devices": devices,

        "encoder": {
            "encoder_dropout_rate": encoder_dropout_rate,
            "encoder_num_layers": encoder_num_layers,
        },

        "classifier": {
            "classifier_dropout_rate": classifier_dropout_rate,
            "classifier_num_layers": classifier_num_layers,
            "bi_reduce_type": str(bi_reduce_type),
            "classifier_type": str(classifier_type),
            "classifier_variational_dropout": classifier_variational_dropout
        },

        "datasets": {
          "train_path": str(train_path),
          "val_path": str(val_path),
          "test_path": str(test_path),
          "seqs_path": str(seqs_path),
          "dataset_path": str(dataset_path),
          "trunc_len": trunc_len,
          "embedding_size": embedding_size,
          "embedding_droprate": embedding_droprate,
          "vocab_size": vocab_size,
        },

        "optimizer": {
            "batch_size": batch_size,
            "optimizer_num_epochs": optimizer_num_epochs,
            "optimizer_lr": optimizer_lr,
            "optimizer_weight_decay": optimizer_weight_decay,
            "optimizer_type": str(optimizer_type),
            "optimizer_swa": optimizer_swa
        }
    }

    args_file = args_path / f'{model_name}.toml'
    console.log(f'Saving args to {args_file}')

    checkpoint_callback = ModelCheckpoint(f"{log_path}/chkpts/", monitor='val_loss', filename=model_name)

    callbacks = [checkpoint_callback]

    if optimizer_swa:
        callbacks.append(StochasticWeightAveraging(swa_lrs=optimizer_lr))

    with open(args_file, 'w') as f:
        toml.dump(args, f)

    dict_logger = DictLogger()

    try:
        tb_logger = TensorBoardLogger(f'{log_path}/tb_logs', name='RAPPPID', version=model_name)

        trainer = pl.Trainer(accelerator=accelerator, devices=devices, max_epochs=optimizer_num_epochs,
                             logger=[tb_logger, dict_logger], precision=16, callbacks=callbacks)
        trainer.fit(model, data_module)

    except KeyboardInterrupt:
        console.log('RAPPPID was Interrupted.')

    finally:
        console.log('Before quitting, saving metrics, running test evaluation.')

        console.log(f'Saving metrics to {args_file}')
        metrics = {k: dict_logger.metrics[k] for k in dict_logger.metrics}
        args['metrics'] = metrics

        with open(args_file, 'w') as f:
            toml.dump(args, f)

        console.log(f'Loading best checkpoint to run tests...')

        test_results = test_model(console, chkpt_path / f'{model_name}.ckpt', onnx_path / f'{model_name}.ckpt',
                                  data_module)

        args['test_results'] = test_results

        console.log(f'Adding test_results to {args_file}')

        with open(args_file, 'w') as f:
            toml.dump(args, f)


fire.Fire(main, name='rapppid')
