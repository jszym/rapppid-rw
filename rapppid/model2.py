#    model2.py
#    Contains an implementation of the RAPPPID Architecture
#    -----
#    RAPPPID - A method for the Regularised Automatic Prediction of Protein-Protein Interactions using Deep Learning
#    Copyright (C) 2021-2022  Joseph Szymborski
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from typing import Any
from sklearn.metrics import (
    roc_auc_score,
    average_precision_score,
    accuracy_score,
    roc_curve,
    precision_recall_curve,
)
import numpy as np
import torch
from torch import nn, Tensor
import pytorch_lightning as pl
from enum import Enum
from .utils import WeightDrop, Mish


class BiReduceType(Enum):
    """
    An enum that represents the type of reduction to apply to the last hidden vector of each direction of the AWD-LSTM

    CONCAT results in the two vectors being concatenated to each other.
    MAX results in taking the maximum values of the two vectors.
    MEAN takes the mean of the two vectors.
    LAST only uses one of the two directions.
    """

    CONCAT = 0
    MAX = 1
    MEAN = 2
    LAST = 3


class ClassifierHeadType(Enum):
    """
    An enum that represents the type of reduction to apply to the latent
    """

    CONCAT = 0
    MEAN = 1
    MULT = 2
    SPEARMAN_POS = 3
    SPEARMAN = 4
    MANHATTAN = 5


class OptimizerType(Enum):
    """
    An enum that represents the optimizer to be used during training.
    """

    ADAMW = 0
    RANGER21 = 1


class Encoder(nn.Module):
    def __init__(
        self,
        embedding_size: int,
        rnn_num_layers: int,
        bi_reduce: BiReduceType,
        lstm_dropout_rate: float,
        variational_dropout: bool,
    ):

        super(Encoder, self).__init__()

        self.bi_reduce = bi_reduce

        if bi_reduce == BiReduceType.CONCAT:
            self.rnn = nn.LSTM(
                embedding_size,
                embedding_size // 2,
                rnn_num_layers,
                bidirectional=True,
                batch_first=True,
            )
        elif bi_reduce in [member.value for member in BiReduceType]:
            self.rnn = nn.LSTM(
                embedding_size,
                embedding_size,
                rnn_num_layers,
                bidirectional=True,
                batch_first=True,
            )
        else:
            raise ValueError("Unexpected value for parameter 'bi_reduce'.")

        # same RNN as before, but with DropConnect/WeightDrop
        self.rnn_dc = WeightDrop(
            self.rnn, ["weight_hh_l0"], lstm_dropout_rate, variational_dropout
        )

        self.fc = nn.Linear(embedding_size, embedding_size)
        self.nl = Mish()

    def forward(self, x: Tensor) -> Tensor:
        output, (hn, cn) = self.rnn_dp(x)

        if self.bi_reduce == BiReduceType.CONCAT:
            # Concatenate both directions
            x = hn[-2:, :, :].permute(1, 0, 2).flatten(start_dim=1)
        elif self.bi_reduce == BiReduceType.MAX:
            # Take the max values of each direction for each dimension
            x = torch.max(hn[-2:, :, :], dim=0).values
        elif self.bi_reduce == BiReduceType.MEAN:
            # Take the mean of both directions.
            x = torch.mean(hn[-2:, :, :], dim=0)
        elif self.bi_reduce == BiReduceType.LAST:
            # Just use the last direction
            x = hn[-1:, :, :].squeeze(0)

        x = self.fc(x)
        x = self.nl(x)

        return x


class RAPPPID(pl.LightningModule):
    def __init__(
        self,
        num_codes: int,
        embedding_size: int,
        steps_per_epoch: int,
        num_epochs: int,
        classhead_dropout_rate: float,
        classhead_num_layers: int,
        lr: float,
        weight_decay: float,
        encoder: nn.Module,
        class_head: nn.Module,
        criterion: Any,  # I'm unaware of any specific type for PyTorch loss fcns, so I'm just using Any
        variational_dropout: bool,
        lr_scaling: bool,
        trunc_len: int,
        embedding_droprate: float,
        optimizer_type: OptimizerType,
    ):

        """
        The RAPPPID PyTorch Lightning module.

        :param num_codes: The number of tokens present in the input sequence.
        :param embedding_size: The number of dimensions to use when embedding the input tokens.
        :param steps_per_epoch: The number of steps in each epoch. This is important for LR scheduling.
        :param num_epochs: The maximum number of epochs to train for.
        :param classhead_dropout_rate: The frequency with which activations are to be randomly set to zero.
        :param classhead_num_layers: The number of fully-connected layers to use in the classifier.
        :param lr: The learning rate to use when optimising.
        :param weight_decay: The amount of Weight Decay (L2 normalisation of the weights) to apply.
        :param encoder: The encoder to use in the twin architecture.
        :param class_head: A torch module which makes an interaction prediction based on two protein embeddings.
        :param variational_dropout: Whether or not to use Variational Dropout in the LSTM layer.
        :param lr_scaling: Scale the learning rates according to the length of the protein sequences.
        :param trunc_len: The maximum number of amino acids to accept before truncating.
        :param embedding_droprate: The frequency with which to set a given token embeddings to zeros.
        :param optimizer_type: The type of optimizer to use.
        """

        super(RAPPPID, self).__init__()

        self.lr_scaling = lr_scaling
        self.trunc_len = trunc_len
        self.num_epochs = num_epochs
        self.num_codes = num_codes
        self.steps_per_epoch = steps_per_epoch
        self.embedding_size = embedding_size
        self.classhead_dropout_rate = classhead_dropout_rate
        self.classhead_num_layers = classhead_num_layers
        self.lr = lr
        self.embedding_droprate = embedding_droprate
        self.weight_decay = weight_decay
        self.class_head = class_head
        self.lr_base = lr
        self.fc = nn.Linear(embedding_size, embedding_size)
        self.nl = Mish()
        self.optimizer_type = optimizer_type
        self.encoder = encoder
        self.class_head = class_head
        self.criterion = criterion

    def forward(self, x: Tensor) -> Tensor:
        x = self.encoder(x)

        return x

    def log_metrics(
        self, batch_idx: int, y_hat: Tensor, y: Tensor, log_prefix: str
    ) -> None:
        y_hat_probs = (
            torch.sigmoid(y_hat.flatten().cpu().detach().flatten())
            .numpy()
            .astype(np.float32)
        )
        y_np = y.flatten().cpu().detach().numpy().astype(int)

        try:
            auroc = roc_auc_score(y_np, y_hat_probs)
        except ValueError as e:
            auroc = -1
        try:
            apr = average_precision_score(y_np, y_hat_probs)
        except ValueError as e:
            apr = -1
        try:
            acc = accuracy_score(y_np, (y_hat_probs > 0.5).astype(int))
        except ValueError as e:
            acc = -1

        if batch_idx == 0:
            self.logger.experiment[0].add_pr_curve(
                f"{log_prefix}_pr", y, torch.sigmoid(y_hat), self.current_epoch
            )
            if len(y_hat_probs[y_np == 1]) > 0:
                self.logger.experiment[0].add_histogram(
                    f"{log_prefix}_pos", y_hat_probs[y_np == 1], self.current_epoch
                )

            if len(y_hat_probs[y_np == 0]) > 0:
                self.logger.experiment[0].add_histogram(
                    f"{log_prefix}_neg", y_hat_probs[y_np == 0], self.current_epoch
                )

        self.log(f"{log_prefix}_auroc", auroc, on_step=False, on_epoch=True)
        self.log(f"{log_prefix}_apr", apr, on_step=False, on_epoch=True)
        self.log(f"{log_prefix}_acc", acc, on_step=False, on_epoch=True)

    def training_step(self, batch, batch_idx) -> Tensor:

        if self.lr_scaling:
            # Reset gradients
            opt = self.optimizers()
            opt.zero_grad()

        # Split out protein A, protein B, and the label Y
        a, b, y = batch

        # The latent representations of protein A and B
        z_a = self(a)
        z_b = self(b)

        y = y.reshape((-1, 1)).float()

        y_hat = self.class_head(z_a, z_b).float()

        self.log_metrics(batch_idx, y_hat, y, "train")

        loss = self.criterion(y_hat, y)

        self.log("train_loss", loss, on_step=False, on_epoch=True)
        self.log(
            "train_loss_step",
            loss,
            on_step=True,
            on_epoch=False,
            prog_bar=self.lr_scaling,
        )

        # LR Scaling
        # This branch of logic scales the learning rate according to the length of the sequences
        # It also logs the scaled learning rate and the sequence lengths.
        if self.lr_scaling:
            max_len_a = torch.max(torch.sum(a != 0, axis=1)).item()
            max_len_b = torch.max(torch.sum(b != 0, axis=1)).item()
            total_len = max_len_a + max_len_b

            lr_scaled = self.lr_base * total_len / (self.trunc_len * 2)
            opt.param_groups[0]["lr"] = lr_scaled

            self.log("total_seq_len", total_len, on_step=True, on_epoch=False)
            self.log("lr_scaled", lr_scaled, on_step=True, on_epoch=False)
            self.log("lr_base", self.lr_base, on_step=True, on_epoch=False)

            self.manual_backward(loss)
            opt.step()

        return loss
