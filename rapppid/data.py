#    data.py
#    PyTorch Dataloaders and Datasets for RAPPPID
#    -----
#    RAPPPID - A method for the Regularised Automatic Prediction of Protein-Protein Interactions using Deep Learning
#    Copyright (C) 2021-2022  Joseph Szymborski
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from pathlib import Path
import gzip
import pickle
from random import randint
from typing import Any, Optional, Tuple, List, Dict

import numpy as np
from nptyping import NDArray, Int
import torch
from torch.utils.data import Dataset, DataLoader
import sentencepiece as sp
import pytorch_lightning as pl
import tables as tb


class LegacyRapppidDataset(Dataset):
    def __init__(self, rows: List[Tuple[str, str, int]], seqs: Dict[str, List[int]], model_file: Optional[Path], trunc_len=1000):
        super().__init__()

        self.rows = rows
        self.seqs = seqs
        self.trunc_len = trunc_len

        if model_file:
            self.spm = sp.SentencePieceProcessor(model_file=str(model_file))
        else:
            self.spm = None

    @staticmethod
    def static_encode(trunc_len: int, seq: List[int], spm: Optional[Any], pad: bool = True) -> NDArray[Any, Int]:
        aas = ['PAD', 'A', 'R', 'N', 'D', 'C', 'Q', 'E', 'G', 'H', 'I', 'L', 'K', 'M', 'F', 'P', 'S', 'T', 'W', 'Y',
               'V', 'O', 'U']

        toks = "".join([aas[r] for r in seq[:trunc_len]])

        if spm:
            toks = np.array(spm.encode(toks, enable_sampling=True, alpha=0.1, nbest_size=-1))
        if pad:
            pad_len = trunc_len - len(toks)
            toks = np.pad(toks, (0, pad_len), 'constant')

        return toks

    def encode(self, seq: List[int], use_sp: bool = True, pad: bool = True) -> NDArray[Any, Int]:

        toks = self.static_encode(self.trunc_len, seq, self.spm if use_sp else None, pad)

        return toks

    def __getitem__(self, idx: int) -> Tuple[torch.LongTensor, torch.LongTensor, torch.LongTensor]:

        p1, p2, label = self.rows[idx]

        p1_seq = self.encode(self.seqs[p1], use_sp=True, pad=True)

        p2_seq = self.encode(self.seqs[p2], use_sp=True, pad=True)

        p1_seq = torch.tensor(p1_seq).long()
        p2_seq = torch.tensor(p2_seq).long()
        label = torch.tensor(label).long()

        return p1_seq, p2_seq, label

    def __len__(self) -> int:
        return len(self.rows)


def get_aa_code(aa: str) -> int:
    # Codes based on IUPAC-IUB
    # https://web.expasy.org/docs/userman.html#AA_codes

    aas = ['PAD', 'A', 'R', 'N', 'D', 'C', 'Q', 'E', 'G', 'H', 'I', 'L', 'K', 'M', 'F', 'P', 'S', 'T', 'W', 'Y', 'V',
           'O', 'U']
    wobble_aas = {
        'B': ['D', 'N'],
        'Z': ['Q', 'E'],
        'X': ['A', 'R', 'N', 'D', 'C', 'Q', 'E', 'G', 'H', 'I', 'L', 'K', 'M', 'F', 'P', 'S', 'T', 'W', 'Y', 'V']
    }

    if aa in aas:
        return aas.index(aa)

    elif aa in ['B', 'Z', 'X']:
        # Wobble
        idx = randint(0, len(wobble_aas[aa]) - 1)
        return aas.index(wobble_aas[aa][idx])


def encode_seq(seq: str) -> List[int]:
    return [get_aa_code(aa) for aa in seq]


class RapppidDataset(Dataset):
    def __init__(self, dataset_path: Path, c_type: int, split: str, model_file: Optional[Path], trunc_len: int = 1000):
        super().__init__()

        self.trunc_len = trunc_len
        self.dataset_path = dataset_path
        self.c_type = c_type
        self.split = split

        if self.split in ['test', 'val']:
            self.sampling = False
        else:
            self.sampling = True

        if model_file:
            self.spm = sp.SentencePieceProcessor(model_file=str(model_file))
        else:
            self.spm = None

    @staticmethod
    def static_encode(trunc_len: int, seq: str, spm: Optional[Any], pad: bool = True, sampling=True) \
            -> NDArray[Any, Int]:

        seq = seq[:trunc_len]

        if spm:
            toks = np.array(spm.encode(seq, enable_sampling=sampling, alpha=0.1, nbest_size=-1))
        else:
            toks = np.array(encode_seq(seq), dtype=np.int)

        if pad:
            pad_len = trunc_len - len(toks)
            toks = np.pad(toks, (0, pad_len), 'constant')

        return toks

    def encode(self, seq: str, use_sp: bool = True, pad: bool = True) -> NDArray[Any, Int]:

        return self.static_encode(self.trunc_len, seq, self.spm if use_sp else None, pad, self.sampling)

    def get_sequence(self, name: str) -> str:
        with tb.open_file(str(self.dataset_path)) as dataset:
            seq = dataset.root.sequences.read_where(f'name=="{name}"')[0][1].decode('utf8')
        return seq

    def __getitem__(self, idx) -> Tuple[torch.LongTensor, torch.LongTensor, torch.LongTensor]:

        with tb.open_file(str(self.dataset_path)) as dataset:

            p1, p2, label = dataset.root['interactions'][f'c{self.c_type}'][f'c{self.c_type}_{self.split}'][idx]

        p1 = p1.decode('utf8')
        p2 = p2.decode('utf8')

        p1_seq = self.encode(self.get_sequence(p1), use_sp=True, pad=True)

        p2_seq = self.encode(self.get_sequence(p2), use_sp=True, pad=True)

        p1_seq = torch.tensor(p1_seq).long()
        p2_seq = torch.tensor(p2_seq).long()
        label = torch.tensor(label).long()

        return p1_seq, p2_seq, label

    def __len__(self):
        with tb.open_file(str(self.dataset_path)) as dataset:
            l = len(dataset.root['interactions'][f'c{self.c_type}'][f'c{self.c_type}_{self.split}'])

        return l


class RapppidDataModule(pl.LightningDataModule):

    def __init__(self, batch_size: int, dataset_path: Path, c_type: int, trunc_len: int, workers: int,
                 model_file: Optional[Path], seed: int):
        super().__init__()

        sp.set_random_generator_seed(seed)

        self.batch_size = batch_size
        self.dataset_path = dataset_path

        self.dataset_train = None
        self.dataset_test = None
        self.dataset_val = None

        self.trunc_len = trunc_len
        self.workers = workers

        self.model_file = model_file
        self.c_type = c_type

        self.train = []
        self.test = []
        self.seqs = []

    def setup(self, stage=None):
        self.dataset_train = RapppidDataset(self.dataset_path, self.c_type, 'train', self.model_file, self.trunc_len)
        self.dataset_val = RapppidDataset(self.dataset_path, self.c_type, 'val', self.model_file, self.trunc_len)
        self.dataset_test = RapppidDataset(self.dataset_path, self.c_type, 'test', self.model_file, self.trunc_len)

    def train_dataloader(self):
        return DataLoader(self.dataset_train, batch_size=self.batch_size, num_workers=self.workers, shuffle=True)

    def val_dataloader(self):
        return DataLoader(self.dataset_val, batch_size=self.batch_size, num_workers=self.workers, shuffle=False)

    def test_dataloader(self):
        return DataLoader(self.dataset_test, batch_size=self.batch_size, num_workers=self.workers, shuffle=False)


class LegacyRapppidDataModule(pl.LightningDataModule):

    def __init__(self, batch_size: int, train_path: Path, val_path: Path, test_path: Path, seqs_path: Path,
                 trunc_len: int, workers: int, model_file: Optional[Path], seed: int):
        super().__init__()

        self.train_pairs = None
        self.val_pairs = None
        self.test_pairs = None

        sp.set_random_generator_seed(seed)

        self.batch_size = batch_size
        self.train_path = train_path
        self.test_path = test_path
        self.val_path = val_path
        self.seqs_path = seqs_path

        self.dataset_train = None
        self.dataset_test = None
        self.dataset_val = None

        self.trunc_len = trunc_len
        self.workers = workers

        self.model_file = model_file

        self.train = []
        self.test = []
        self.seqs = []

    def setup(self, stage=None):
        with gzip.open(self.seqs_path) as f:
            self.seqs = pickle.load(f)

        with gzip.open(self.test_path) as f:
            self.test_pairs = pickle.load(f)

        with gzip.open(self.val_path) as f:
            self.val_pairs = pickle.load(f)

        with gzip.open(self.train_path) as f:
            self.train_pairs = pickle.load(f)

        self.dataset_train = LegacyRapppidDataset(self.train_pairs, self.seqs, self.model_file, self.trunc_len)
        self.dataset_val = LegacyRapppidDataset(self.val_pairs, self.seqs, self.model_file, self.trunc_len)
        self.dataset_test = LegacyRapppidDataset(self.test_pairs, self.seqs, self.model_file, self.trunc_len)

    def train_dataloader(self):
        return DataLoader(self.dataset_train, batch_size=self.batch_size,
                          num_workers=self.workers, shuffle=True)

    def val_dataloader(self):
        return DataLoader(self.dataset_val, batch_size=self.batch_size,
                          num_workers=self.workers, shuffle=False)

    def test_dataloader(self):
        return DataLoader(self.dataset_test, batch_size=self.batch_size,
                          num_workers=self.workers, shuffle=False)

