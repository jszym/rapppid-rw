#    metrics.py
#    Implementations for logging metrics
#    -----
#    RAPPPID - A method for the Regularised Automatic Prediction of Protein-Protein Interactions using Deep Learning
#    Copyright (C) 2021-2022  Joseph Szymborski
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from sklearn.metrics import roc_auc_score, average_precision_score, accuracy_score, roc_curve, precision_recall_curve


def compute_metrics(y, y_hat):
    try:
        auroc = roc_auc_score(y, y_hat)
    except ValueError as e:
        auroc = -1
    try:
        apr = average_precision_score(y, y_hat)
    except ValueError as e:
        apr = -1
    try:
        acc = accuracy_score(y, (y_hat > 0.5).astype(int))
    except ValueError as e:
        acc = -1

    return {
        'auroc': auroc,
        'apr': apr,
        'acc': acc
    }
