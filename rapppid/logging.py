#    logging.py
#    Logging tools for RAPPPID
#    -----
#    RAPPPID - A method for the Regularised Automatic Prediction of Protein-Protein Interactions using Deep Learning
#    Copyright (C) 2021-2022  Joseph Szymborski
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from pytorch_lightning.loggers.logger import Logger, rank_zero_experiment
from pytorch_lightning.utilities import rank_zero_only
from collections import defaultdict


class DictLogger(Logger):

    @rank_zero_only
    def __init__(self):
        super().__init__()
        # Defining the dict
        self.metrics = defaultdict(self.def_value)

    @rank_zero_only
    def def_value(self):
        return []

    @property
    def name(self):
        return "DictLogger"

    @property
    def version(self):
        return "0.1"

    @rank_zero_only
    def log_hyperparams(self, params):
        # params is an argparse.Namespace
        # your code to record hyperparameters goes here
        pass

    @rank_zero_only
    def log_metrics(self, metrics, step):
        # metrics is a dictionary of metric names and values
        # your code to record metrics goes here
        for key in metrics.keys():
            self.metrics[key].append(metrics[key])
