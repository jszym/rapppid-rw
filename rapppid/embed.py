#    embed.py
#    Contains an implementation of the Twin Neural Network Architecture
#    -----
#    RAPPPID - A method for the Regularised Automatic Prediction of Protein-Protein Interactions using Deep Learning
#    Copyright (C) 2021-2022  Joseph Szymborski
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from torch import nn, Tensor
import torch.nn.functional as F
from strenum import StrEnum

from enum import auto
from typing import Any


class EmbedderType(StrEnum):
    """
    Represents the different Embedders

    DROP represents the DropEmbedder which implements Embedding Dropout
    """

    DROP = auto()


class Embedder:
    def __init__(self, num_codes: int, embedding_size: int):
        self.num_codes = num_codes
        self.embedding_size = embedding_size


    def embed(self, x: Tensor, training: bool) -> Tensor:
        raise NotImplemented


class DropEmbedder(Embedder):
    def __init__(self, num_codes, embedding_size, embedding_droprate):
        super().__init__(num_codes, embedding_size)
        self.embedding_droprate = embedding_droprate

    def embed(self, x: Tensor, training: bool) -> Tensor:

        embed = nn.Embedding(self.num_codes, self.embedding_size, padding_idx=0).to(x.device)

        if not training:
            masked_embed_weight = embed.weight
        elif not self.embedding_droprate:
            masked_embed_weight = embed.weight
        else:
            mask = embed.weight.data.new().resize_((embed.weight.size(0), 1)).bernoulli_(1 - self.embedding_droprate).expand_as(
                embed.weight) / (1 - self.embedding_droprate)
            masked_embed_weight = mask * embed.weight

        padding_idx = embed.padding_idx
        if padding_idx is None:
            padding_idx = -1

        X = F.embedding(x, masked_embed_weight,
                        padding_idx, embed.max_norm, embed.norm_type,
                        embed.scale_grad_by_freq, embed.sparse)
        return X

