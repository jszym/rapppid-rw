#    rapppid.py
#    Contains an implementation of the RAPPPID Neural Network
#    -----
#    RAPPPID - A method for the Regularised Automatic Prediction of Protein-Protein Interactions using Deep Learning
#    Copyright (C) 2021-2022  Joseph Szymborski
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import pytorch_lightning as pl
from torch import nn
import torch
import numpy as np
from ranger21 import Ranger21

from typing import Optional, Tuple

from .optimizer import OptimizerType
from .encoders import EncoderType
from .encoders.awd_lstm import AWDLSTM, BiReduceType
from .embed import EmbedderType, DropEmbedder
from .twin_net import TwinNet
from .classifier import ClassifierType
from .classifier.mult import MultClassHead
from .metrics import compute_metrics


def sigmoid(z):
    return 1 / (1 + np.exp(-z))


class RAPPPID(pl.LightningModule):
    def __init__(
        self,
        embedder_type: EmbedderType,
        encoder_type: EncoderType,
        classifier_type: ClassifierType,
        optimizer_type: OptimizerType,
        vocab_size: int,
        embedding_size: int,
        optimizer_lr: float,
        bi_reduce_type: Optional[BiReduceType] = None,
        encoder_num_layers: Optional[int] = None,
        encoder_dropout_rate: Optional[float] = None,
        encoder_variational_dropout: Optional[bool] = None,
        embedding_droprate: Optional[float] = None,
        classifier_num_layers: Optional[int] = None,
        classifier_dropout_rate: Optional[int] = None,
        classifier_variational_dropout: Optional[bool] = None,
        optimizer_weight_decay: Optional[float] = None,
        optimizer_steps_per_epoch: Optional[int] = None,
        optimizer_num_epochs: Optional[int] = None
    ):

        """
        The RAPPPID PyTorch Lightning module.

        """

        super(RAPPPID, self).__init__()

        self.embedder_type = embedder_type
        self.encoder_type = encoder_type
        self.classifier_type = classifier_type
        self.optimizer_type = optimizer_type
        self.vocab_size = vocab_size
        self.embedding_size = embedding_size
        self.bi_reduce_type = bi_reduce_type
        self.encoder_num_layers = encoder_num_layers
        self.embedding_droprate = embedding_droprate
        self.encoder_dropout_rate = encoder_dropout_rate
        self.classifier_num_layers = classifier_num_layers
        self.classifier_dropout_rate = classifier_dropout_rate
        self.classifier_variational_dropout = classifier_variational_dropout
        self.optimizer_lr = optimizer_lr
        self.optimimzer_weight_decay = optimizer_weight_decay
        self.optimizer_steps_per_epoch = optimizer_steps_per_epoch
        self.optimizer_num_epochs = optimizer_num_epochs

        if embedder_type == EmbedderType.DROP:

            if embedding_droprate is None or not isinstance(embedding_droprate, float) or embedding_droprate < 0 \
                    or embedding_droprate > 1:
                raise ValueError('embedding_droprate must be a float between 0 and 1 if embedder_type is DROP')

            self.embedder = DropEmbedder(vocab_size, embedding_size, embedding_droprate)

        if encoder_type == EncoderType.AWDLSTM:

            if encoder_num_layers is None:
                raise ValueError('encoder_num_layers must be an integer if encoder_type is AWDLSTM')

            if encoder_variational_dropout is None:
                raise ValueError('encoder_variational_dropout must be a bool if encoder_type is AWDLSTM')

            if bi_reduce_type is None:
                raise ValueError('bi_reduce_type must not be None if encoder_type is AWDLSTM')

            if encoder_dropout_rate is None:
                raise ValueError('encoder_dropout_rate must float if encoder_type is AWDLSTM')

            self.encoder = AWDLSTM(self.embedder, bi_reduce_type, encoder_num_layers, encoder_dropout_rate,
                                   encoder_variational_dropout)

        if classifier_type == ClassifierType.MULT:

            if classifier_num_layers is None:
                raise ValueError('classifier_num_layers must be an integer if classifier_type is MULT')

            if classifier_dropout_rate is None:
                raise ValueError('classifier_dropout_rate must be a float if classifier_type is MULT')

            if classifier_variational_dropout is None:
                raise ValueError('classifier_variational_dropout must be a bool if classifier_type is MULT')

            self.classifier = MultClassHead(embedding_size, classifier_num_layers, classifier_dropout_rate,
                                            classifier_variational_dropout)

        self.twin = TwinNet(self.encoder, self.classifier)

        self.criterion = nn.BCEWithLogitsLoss()

    def forward(self, a: torch.Tensor, b: torch.Tensor) -> Tuple[torch.Tensor, torch.Tensor, torch.Tensor]:

        z_a, z_b, y_hat_logit = self.twin(a, b)

        return z_a, z_b, y_hat_logit

    def step(self, batch, batch_idx, split):

        a, b, y = batch

        z_a, z_b, y_hat_logit = self(a, b)

        y = y.reshape((-1, 1)).float()
        loss = self.criterion(y_hat_logit, y)

        y_hat = torch.sigmoid(y_hat_logit.flatten().cpu().detach().flatten()).numpy().astype(np.float32)
        y_np = y.flatten().cpu().detach().numpy().astype(int)

        metrics = compute_metrics(y_np, y_hat)

        self.log(f'{split}_auroc', metrics['auroc'], on_step=False, on_epoch=True)
        self.log(f'{split}_apr', metrics['apr'], on_step=False, on_epoch=True)
        self.log(f'{split}_acc', metrics['acc'], on_step=False, on_epoch=True)

        if batch_idx == 0:
            self.logger.experiment.add_pr_curve('train_pr', y, torch.sigmoid(y_hat_logit), self.current_epoch)
            if len(y_hat[y_np == 1]) > 0:
                self.logger.experiment.add_histogram('train_pos', y_hat_logit[y_np == 1], self.current_epoch)

            if len(y_hat[y_np == 0]) > 0:
                self.logger.experiment.add_histogram('train_neg', y_hat_logit[y_np == 0], self.current_epoch)

        self.log(f'{split}_loss', loss, on_step=False, on_epoch=True, prog_bar=split == 'val')
        self.log(f'{split}_step', loss, on_step=True, on_epoch=False)

        return loss

    def training_step(self, batch, batch_idx) -> torch.Tensor:

        loss = self.step(batch, batch_idx, 'train')

        return loss

    def validation_step(self, batch, batch_idx) -> torch.Tensor:

        loss = self.step(batch, batch_idx, 'val')

        return loss

    def test_step(self, batch, batch_idx) -> torch.Tensor:

        loss = self.step(batch, batch_idx, 'test')

        return loss

    def configure_optimizers(self):

        if self.optimizer_type == OptimizerType.RANGER21:

            if self.optimimzer_weight_decay is None:
                raise ValueError('optimizer_weight_decay must be a float if optimizer_type is RANGER21')

            if self.optimizer_steps_per_epoch is None:
                raise ValueError('optimizer_steps_per_epoch must be an integer if optimizer_type is RANGER21')

            if self.optimizer_num_epochs is None:
                raise ValueError('optimizer_num_epochs must be an integer if optimizer_type is RANGER21')

            optimizer = Ranger21(self.parameters(),
                                 lr=self.optimizer_lr,
                                 weight_decay=self.optimimzer_weight_decay,
                                 num_batches_per_epoch=self.optimizer_steps_per_epoch,
                                 num_epochs=self.optimizer_num_epochs,
                                 warmdown_start_pct=0.72)

        elif self.optimizer_type == OptimizerType.ADAMW:
            optimizer = torch.optim.AdamW(self.parameters(), lr=self.optimizer_lr,
                                          weight_decay=self.optimimzer_weight_decay)

        else:
            raise ValueError(f'Unexpected value for optimizer_type: {self.optimizer_type}')

        return optimizer

