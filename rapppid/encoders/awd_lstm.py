#    awd_lstm.py
#    Defines the Average Weight-Dropped Long Short-Term Memory Architecture
#    This AWD-LSTM is slightly different from the original Merity LSTM to suit
#    our context.
#    -----
#    RAPPPID - A method for the Regularised Automatic Prediction of Protein-Protein Interactions using Deep Learning
#    Copyright (C) 2021-2022  Joseph Szymborski
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from torch import nn, Tensor
import torch
from strenum import StrEnum

from enum import auto

from ..encoders import Encoder
from ..embed import Embedder
from ..utils import WeightDrop, Mish


class BiReduceType(StrEnum):
    """
    An enum that represents the type of reduction to apply to the last hidden vector of each direction of the AWD-LSTM

    CONCAT results in the two vectors being concatenated to each other.
    MAX results in taking the maximum values of the two vectors.
    MEAN takes the mean of the two vectors.
    LAST only uses one of the two directions.
    """

    CONCAT = auto()
    MAX = auto()
    MEAN = auto()
    LAST = auto()


class AWDLSTM(Encoder):
    def __init__(self,
                 embedder: Embedder,
                 bi_reduce_type: BiReduceType,
                 num_layers: int,
                 dropout_rate: float,
                 variational_dropout: float):
        super(AWDLSTM, self).__init__(embedder)

        self.embedder = embedder
        self.bi_reduce_type = bi_reduce_type

        embedding_size = self.embedder.embedding_size

        if self.bi_reduce_type == BiReduceType.CONCAT:
            self.rnn = nn.LSTM(embedding_size, embedding_size // 2, num_layers, bidirectional=True,
                               batch_first=True)
        elif self.bi_reduce_type in [BiReduceType.MAX, BiReduceType.MEAN, BiReduceType.LAST]:
            self.rnn = nn.LSTM(embedding_size, embedding_size, num_layers, bidirectional=True, batch_first=True)
        else:
            raise ValueError(f"Unexpected value for `bi_reduce` {bi_reduce_type}")

        self.rnn_dp = WeightDrop(
            self.rnn, ['weight_hh_l0'], dropout_rate, variational_dropout
        )

        self.fc = nn.Linear(embedding_size, embedding_size)
        self.nl = Mish()

    def forward(self, x: Tensor) -> Tensor:

        x = self.embedder.embed(x, self.training)

        output, (hn, cn) = self.rnn_dp(x)

        if self.bi_reduce_type == BiReduceType.CONCAT:
            # Concat both directions
            x = hn[-2:, :, :].permute(1, 0, 2).flatten(start_dim=1)
        elif self.bi_reduce_type == BiReduceType.MAX:
            # Max both directions
            x = torch.max(hn[-2:, :, :], dim=0).values
        elif self.bi_reduce_type == BiReduceType.MEAN:
            # Mean both directions
            x = torch.mean(hn[-2:, :, :], dim=0)
        elif self.bi_reduce_type == BiReduceType.LAST:
            # Just use last direction
            x = hn[-1:, :, :].squeeze(0)

        x = self.fc(x)
        x = self.nl(x)

        return x
