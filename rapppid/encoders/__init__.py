#    encoders
#    Defines the Encoder class
#    -----
#    RAPPPID - A method for the Regularised Automatic Prediction of Protein-Protein Interactions using Deep Learning
#    Copyright (C) 2021-2022  Joseph Szymborski
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from torch import nn
from strenum import StrEnum

from enum import auto

from ..embed import Embedder


class EncoderType(StrEnum):
    """
    Represents the different Encoders

    AWDLSTM represents the AWD-LSTM encoder
    """

    AWDLSTM = auto()


class Encoder(nn.Module):
    def __init__(self, embedder: Embedder):
        super(Encoder, self).__init__()

        self.embedder = embedder

    def forward(self, x):
        raise NotImplemented
